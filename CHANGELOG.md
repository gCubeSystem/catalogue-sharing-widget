
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.1] - 2021-10-27

- [#24038] Moved to GWT 2.9.0

## [v1.2.0] - 2021-04-12

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20735] Bug fixes


## [v1.1.0] - 2019-12-05

Bug fixes


## [v1.0.1] - 2017-05-10

Bug fixes


## [v1.0.0] - 2017-05-01

First release
